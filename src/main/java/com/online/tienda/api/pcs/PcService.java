package com.online.tienda.api.pcs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class PcService {
    @Autowired
    private PcRepository pcRepository;

    public List<PcDto> getAllPcs() {
        List<Pc> pcs = pcRepository.findAll();
        return pcs.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private PcDto convertToDto(Pc pc) {
        PcDto pcDto = new PcDto();
        pcDto.setCodigo(pc.getCodigo());
        pcDto.setTitulo(pc.getTitulo());
        pcDto.setPrecio(pc.getPrecio());
        pcDto.setStock(pc.getStock());
        return pcDto;
    }
}