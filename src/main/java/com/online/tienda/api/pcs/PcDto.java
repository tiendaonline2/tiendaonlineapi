package com.online.tienda.api.pcs;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PcDto {
    private Long codigo;
    private String titulo;
    private BigDecimal precio;
    private Integer stock;

    // Getters y setters
}
