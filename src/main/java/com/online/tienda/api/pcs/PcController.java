package com.online.tienda.api.pcs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pcs")
public class PcController {
    @Autowired
    private PcService pcService;

    @GetMapping
    public List<PcDto> getAllPcs() {
        return pcService.getAllPcs();
    }
}